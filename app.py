import os

from trends.app import create_app

if __name__ == '__main__':
    # export DATABASE_URL=postgresql://me:hackme@0.0.0.0/citizens
    print('DATABASE_URL', os.environ['DATABASE_URL'])

    app = create_app(os.environ['DATABASE_URL'])
    app.run(host='0.0.0.0', port=8080)
